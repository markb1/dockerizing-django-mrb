#!/bin/bash 
set -e
echo "Creating DB"
psql <<- EOSQL
    CREATE DATABASE dockerizing;
    CREATE USER django PASSWORD 'CHANGE.ME!pgsql';
    GRANT ALL PRIVILEGES ON DATABASE dockerizing TO django;
EOSQL
echo "Done Creating DB"
