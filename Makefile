# NOTE: On Ubuntu Linux, can use "remake" to run this Makefile
#   alias make=remake
#
# NOTE: must use real Tabs, not spaces, for Makefile commands!
#

# alias dc=docker-compose
# dc up -d web; dc logs web
# dc logs postgres

.PHONY: functests migrate_full

.ONESHELL:

DC = docker-compose

xtra_clean:
	docker image remove 'dockerizing-django_web:latest'

clean_web:
	${DC} stop web && ${DC} rm web
	find . -type d -name __pycache__ | sudo xargs /bin/rm -rf

build: clean_web
	script -c "${DC} build" compilation.$${$$}

start_db:
	${DC} up -d db

migrate: start_db
	${DC} exec web  python manage.py makemigrations
	${DC} exec web  python manage.py migrate

migrate_todo:
	${DC} exec web  python manage.py makemigrations todo
	${DC} exec web  python manage.py migrate todo

setpass:
	${DCa} exec web  python manage.py createsuperuser

up:
	${DC} up -d

logs_web:
	${DC} logs -f web

down:
	${DC} stop ${APP}

run_web:
	${DC} run web python3 manage.py runserver 0.0.0.0:8000

# good for debugging, above 'web' targets
run_http:
	${DC} run -d web python3 -m http.server 9999

install_docker_compose:
	curl -L https://github.com/docker/compose/releases/download/1.8.0/docker-compose-$(uname -s)-$(uname -m) > /usr/local/bin/docker-compose
	chmod +x /usr/local/bin/docker-compose

