import os
import json
import warnings

from collections.abc import MutableMapping

# Ref:
# https://pymotw.com/2/abc/


@MutableMapping.register
class GenMap(object):
    """ """
    def __init__(self, *args, **kw):
        self.__dict__ = dict(*args, **kw)

    def __getitem__(self, key):
        return self.__dict__[key]

    def __setitem__(self, key, val):
        self.__dict__[key] = val

    def __delitem__(self, key):
        del self.__dict__[key]

    def __iter__(self):
        return iter(self.__dict__)

    def __len__(self):
        return len(self.__dict__)

    def keys(self):
        return self.__dict__.keys()

# MutableMapping.register(GenMap)


def get_django_env():
    """ """
    runtime = os.environ.get('ENV', 'dev')

    jsonf = os.path.join('env_' + runtime, 'secrets.json')
    try:
        with open(jsonf) as f:
            jsd = json.loads(f.read())
    except json.decoder.JSONDecodeError as e:
        print("file: {}, =>> \n{} ".format(jsonf, e))
        raise RuntimeError

    menv = GenMap(**jsd)
    return menv


if __name__ == "__main__":
    env = get_django_env()

    for key in env.keys():
        try:
            print(key, env[key])
        except KeyError:
            msg = "No value for <{0}> environ-key".format(key)
            warnings.warn(msg)
