#!/usr/bin/env python
"""Django's command-line utility for administrative tasks."""
import os
import sys

from django.conf import settings
from base import get_django_env


def main():
    # SEE: https://docs.djangoproject.com/en/2.2/topics/settings/
    # os.environ.setdefault("DJANGO_SETTINGS_MODULE", "docker_django.settings")

    env = get_django_env()

    BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

    # settings.py, dumped here! (what's normally in)
    settings.configure(
        DEBUG=env.DEBUG,
        # docker inspect dockerizing-django_default | egrep Gateway | awk '{print $2}'
        ALLOWED_HOSTS=env.ALLOWED_HOSTS,
        SECRET_KEY=env.SECRET_KEY,

        BASE_DIR=BASE_DIR,
        SITE_ROOT=os.path.dirname(os.path.realpath(__file__)),

        MIDDLEWARE=(
            'django.contrib.sessions.middleware.SessionMiddleware',
            'django.middleware.common.CommonMiddleware',
            'django.middleware.csrf.CsrfViewMiddleware',
            'django.contrib.auth.middleware.AuthenticationMiddleware',
            'django.contrib.messages.middleware.MessageMiddleware',
            'django.middleware.clickjacking.XFrameOptionsMiddleware',
            'django.middleware.security.SecurityMiddleware',
        ),

        INSTALLED_APPS=(
            'django.contrib.admin',
            'django.contrib.auth',
            'django.contrib.contenttypes',
            'django.contrib.sessions',
            'django.contrib.messages',
            'django.contrib.staticfiles',
            # apps
            'docker_django.apps.todo',
        ),

        ROOT_URLCONF='docker_django.urls',

        TEMPLATES=[
            {
                'BACKEND': 'django.template.backends.django.DjangoTemplates',
                'DIRS': [],
                'APP_DIRS': True,
                'OPTIONS': {
                    'context_processors': [
                        'django.template.context_processors.debug',
                        'django.template.context_processors.request',
                        'django.contrib.auth.context_processors.auth',
                        'django.contrib.messages.context_processors.messages', ],
                },
            },
        ],
        WSGI_APPLICATION='docker_django.wsgi.application',
        DATABASES={
            'default': {
                'ENGINE': 'django.db.backends.postgresql_psycopg2',
                'NAME': env.DB_NAME,
                'USER': env.DB_USER,
                'PASSWORD': env.DB_PASS,
                'HOST': env.DB_SERVICE,
                'PORT': env.DB_PORT,
            }
        },
        LANGUAGE_CODE='en-us',
        TIME_ZONE='UTC',
        USE_I18N=True,
        USE_L10N=True,
        USE_TZ=True,

        STATIC_URL='/static/',
        STATIC_ROOT=os.path.join(BASE_DIR, 'static'),
    )

    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)


if __name__ == '__main__':
    main()
