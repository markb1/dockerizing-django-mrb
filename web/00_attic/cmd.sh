#!/bin/sh

# web/cmd.sh

set -e

if [ "$ENV" = 'dev' ]; then
    echo "running docker_django in 'dev' mode..."
#    exec python3 manage.py runserver 0.0.0.0:8000
    exec python3 /usr/src/app/lw_hello.py runserver 0.0.0.0:9999
else
    echo "running docker_django in 'Production' mode..."
    exec uwsgi --http 0.0.0.0:9090 --wsgi-file /usr/src/app/lw_hello.py \
	--callable app --stats 0.0.0.0:9191
fi

# python manage.py runserver 0.0.0.0:8000
# And if that works, run it using uWSGI:
# uwsgi --http :8000 --module mysite.wsgi
