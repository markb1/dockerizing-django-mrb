#!/usr/bin/env python3

from importd import d
d(DEBUG=True)


@d("/")
def idx(request):
    return "index.html"


@d("/post/<int:post_id>/")
def post(request, post_id):
    return "post.html", {"post_id": post_id}

if __name__ == "__main__":
    d.main()
